import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Card } from '../../models/card';

/*
  Generated class for the CardProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CardProvider {

  constructor(public http: HttpClient) {

  }

  getAll() {
    let cards = new Array();
    let suits = ['clubs', 'diamond', 'hearts', 'spades'];
    let names = ['ace', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'jack', 'queen', 'king'];

    for (let n = 0; n < names.length; n++) {
      for (let s = 0; s < suits.length; s++) {
        cards.push(new Card({ name: names[n], suit: suits[s], used: false, user: '' }));
      }
    }

    return cards;
  }

  getUseds() {

  }

  postUsed(card: Card) {

  }

  clean() {

  }
}
