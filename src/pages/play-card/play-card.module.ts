import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlayCardPage } from './play-card';

@NgModule({
  declarations: [
    PlayCardPage,
  ],
  imports: [
    IonicPageModule.forChild(PlayCardPage),
  ],
})
export class PlayCardPageModule {}
