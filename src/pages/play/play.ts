import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Card } from '../../models/card';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

@IonicPage()
@Component({
  selector: 'page-play',
  templateUrl: 'play.html',
})
export class PlayPage {

  cards: Array<Card> = [];
  card: Card;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController) {
    this.card = new Card({ image: 'assets/imgs/black_joker.png', used: false, user: '' });

    let suits = ['clubs', 'diamonds', 'hearts', 'spades'];
    let names = ['ace', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'jack', 'queen', 'king'];

    for (let n = 0; n < names.length; n++) {
      for (let s = 0; s < suits.length; s++) {
        this.cards.push(new Card({ image: `assets/imgs/${names[n]}_of_${suits[s]}.png`, used: false, user: '' }));
      }
    }
  }

  sort() {
    let randomCard = 0;

    let interval = setInterval(() => {
      randomCard = Math.floor(Math.random() * this.cards.length);

      this.card = this.cards[randomCard];
    }, 100);

    setTimeout(() => {
      clearInterval(interval);
      this.cards.splice(randomCard, 1);
      this.cards.sort();
      this.showRule();
    }, 2000);
  }

  showRule() {
    if (this.cards.length < 52) {
      let name = this.card.image.split('_')[0];
      let rules = [''];

      this.alertCtrl.create({
        title: 'Regra',
        message: rules[name],
        buttons: [
          {
            text: 'OK'
          }
        ]
      }).present();
    }
  }

}
