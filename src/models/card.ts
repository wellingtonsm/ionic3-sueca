export class Card {
    image: string;
    used: boolean;
    user: string;

    constructor(obj: Card) {
        this.image = obj.image;
        this.used = obj.used;
        this.user = obj.user;
    }
}